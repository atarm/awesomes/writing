<!--
---
name: "📚 docs bug report"
about: report a docs bug
---
🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅

oh hi there! 😄

to expedite issue processing please search open and closed issues before submitting a new one.

existing issues often contain information about workarounds, resolution, or progress updates.

🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅🔅
-->

<!--
borrow from:

https://raw.githubusercontent.com/angular/angular/master/.github/ISSUE_TEMPLATE/3-docs-bug.md

-->

# 📚 Docs Bug Report

## :earth_asia: What is The Affected URL?

<!-- :memo: -->

## :incoming_envelope: Short Description

<!-- :memo: a clear and concise description of the problem... -->

## :camera: Snapshoot

<!-- :memo: often a screenshot can help to capture the issue better than a long description. -->

<!-- :memo: upload a screenshot:-->

## :slightly_smiling_face: Expected

<!--- :memo: tell us what should happen -->

## :upside_down_face: Actual

<!--- :memo: tell us what happens instead of the expected behavior -->

## :angry: Is This a Regression?

<!-- :memo: did this behavior use to work in the previous version? -->

<!-- :memo: yes, the previous version in which this bug was not present was: .... -->

## :link: Anything Else Relevant?

<!-- :memo: please provide additional info if necessary. -->
