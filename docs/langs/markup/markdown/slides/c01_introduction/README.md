---
marp: true
theme: gaia
class: invert + lead
size: 16:9
auto-scaling: true
paginate: true
color: white
backgroundColor: #202228
header: "_Introduction to Markdown_"
footer: "@aRoming"
math: katex

---
<!--1st leading page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->

# Introduction to Markdown

>1. `Markdown Reference` of `Typora`
>2. [QuickRef.ME. Markdown.](https://quickref.me/markdown)
>3. [GitHub. Basic writing and formatting syntax.](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

---
<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Markdown介绍](#markdown介绍)
2. [Markdown语法](#markdown语法)

<!-- /code_chunk_output -->

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Markdown介绍

>1. [wiki. Markdown.](https://en.wikipedia.org/wiki/Markdown)

---

### Markdown是什么

1. `John Gruber` created `Markdown` in March 9, 2004 as a `markup language` that is easy to read in its source code form
2. `Markdown`是一种轻量级标记语言（`LightWeight Markup Language`）
3. `Markdown`可以看成是`HTML`的一种简化
4. `Markdown`跟`HTML`一样，以纯文本文件形式存储，后缀名一般为`.md`或`.markdown`
5. `Markdown`一般渲染成`HTML`形式，也可以渲染成`PDF`、`Word`等形式

---

1. `Markdown`是一种开源的语法，对其有不少扩展，`GFM(GitHub Flavored Markdown)`是其中重要的扩展之一
1. `Markdown`是`GitHub`默认支持的语法，伴随着`GitHub`的流行，`Markdown`也越来越流行

---

### Markdown的优势

1. 纯文本
    1. 兼容性强：可使用任意文本编辑器打开
    1. VCS友好：可使用`Git`等进行版本管理
1. 语法简单：易于学习和使用，专注于内容
1. `CSS`排版：可使用`CSS`进行排版，可定制性强
1. 跨平台：可在`Windows`、`Linux`、`Mac`等平台使用
1. 主流支持：主流的开发工具、编辑器、博客平台等均支持`Markdown`

---

1. `HTML`兼容：可在`Markdown`中直接使用`HTML`语法
1. 工具链丰富
    1. 整合`GraphViz`、`PlantUML`、`GitBook`等工具
    1. 整合`LaTeX`、`MathJax`等数学工具
1. 编程语言友好
    1. `Markdown`对编程语言的支持：语法高亮、代码块、代码行号等
    1. 编程语言对`Markdown`的支持：`Python`、`R`等编程语言都有对应的`Markdown`解析器

---

### Markdown的劣势

1. 语法有限：`Markdown`的语法有限，不适合复杂的排版
1. 语法不统一：不同的`Markdown`解析器对语法的支持不同（但大多数均支持`GFM`）

---

### Markdown适用的场景

1. 博客文档
1. 电子书
1. 笔记
1. 软件说明书

---

### Markdown不适用的场景

1. 纸质出版物（纸质书籍、论文等）
1. 行政文档（公司公文等）
1. 格式样式复杂的文档（大量不同字体、复杂表格等）
1. 版式复杂的文档（大量分栏、不同章节不同版式等）

---
<!--2nd leading page-->
<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## Markdown语法

---

### Markdown的语法标准

1. [John Gruber. Markdown.](https://daringfireball.net/projects/markdown/): Latest release v1.0.1, in December 17, 2004
2. In September 2014, `John Gruber` objected to the usage of `"Markdown"` in the name of this effort and it was rebranded as `CommonMark`
3. [John MacFarlane. CommonMark Spec.](https://spec.commonmark.org/)
4. [GitHub Flavored Markdown Spec](https://github.github.com/gfm/)

---

### Markdown的语法教程

1. `Markdown Reference` of `Typora`
2. [QuickRef.ME. Markdown.](https://quickref.me/markdown)
3. [GitHub. Basic writing and formatting syntax.](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

---
<!--end of slide page-->
<!--
_backgroundColor: White
_color:
_class:
    - lead
-->
✨✨✨ **Thank You** ✨✨✨

🆗 **End of This Slide** 🆗
