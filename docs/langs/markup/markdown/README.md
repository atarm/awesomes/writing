# Markdown

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What is Markdown?](#-what-is-markdown)
2. [When Should Use Markdown and When Should not Use?](#-when-should-use-markdown-and-when-should-not-use)
    1. [Why Markdown is So Popular?](#-why-markdown-is-so-popular)
    2. [What are Markdown Weak in (When Should not Use Markdown)?](#-what-are-markdown-weak-in-when-should-not-use-markdown)
    3. [When Should Use Markdown?](#-when-should-use-markdown)
3. [How to Learn Markdown?](#-how-to-learn-markdown)
4. [Futhermore](#-futhermore)
    1. [Standards and Specifications](#-standards-and-specifications)
    2. [Documents and Supports](#-documents-and-supports)
    3. [Papers and Articles](#-papers-and-articles)
    4. [Books and Monographs](#-books-and-monographs)
    5. [Courses and Tutorials](#-courses-and-tutorials)
    6. [Manuals and CheatSheets](#-manuals-and-cheatsheets)
    7. [Playgrounds and Exercises](#-playgrounds-and-exercises)
    8. [Examples and Templates](#-examples-and-templates)
    9. [Auxiliaries and Tools](#-auxiliaries-and-tools)
    10. [Miscellaneous](#-miscellaneous)

<!-- /code_chunk_output -->

## What is Markdown?

>`Markdown` is a text-to-HTML conversion tool for web writers. `Markdown` allows you to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML).
>
>by John Gruber at <https://daringfireball.net/projects/markdown/>

>`Markdown` is a lightweight markup language with plain-text-formatting syntax, created in 2004 by John Gruber with Aaron Swartz. `Markdown` is often used to format readme files, for writing messages in online discussion forums, and to create rich text using a plain text editor.
>
>by wikipedia at <https://en.wikipedia.org/wiki/Markdown>

1. 2004: John Gruber created the `Markdown` language in collaboration with Aaron Swartz on the syntax
1. 2008/02: `GitHub` service started
1. 2010/05: ReadWriteWeb reported that `GitHub` had surpassed `SourceForge` and `Google Code` in total number of commits for the period of January to May 2011
1. 2012: a group of people, including Jeff Atwood and John MacFarlane, launched what Atwood characterized as a standardization effort
1. 2014/09: John Gruber objected to the usage of "`Markdown`" in the name of standardization effort and it was rebranded as a new dialect named `CommonMark`, `CommonMark` current latest version is 0.29(2019-04-06)
1. 2017: `GitHub` released a formal specification of their `GitHub Flavored Markdown (GFM)` that is based on `CommonMark`, `GFM` is a strict superset of CommonMark

>1. [Markdown](https://en.wikipedia.org/wiki/Markdown)
>1. [Github](https://en.wikipedia.org/wiki/GitHub)
>1. [CommonMark Spec](https://spec.commonmark.org/)

## When Should Use Markdown and When Should not Use?

### Why Markdown is So Popular?

1. VCS-friendly易于版本控制： 区别于二进制文档/富文本文档
1. keyboard-friendly键盘友好： 区别于`Visual Editor`(mouse-used)
1. simple and enough简单够用： 区别于宏脚本文档
1. easy read and easy write易读易写： 区别于`HTML`
1. easy render with `CSS`易渲染： 区别于二进制文档
1. `Stack Overflow`, `Reddit`, `GitHub` and many famous promoted： 区别于其他`LML`
1. extend-friendly易于扩展： 集成`script drawing`、`tex formula`、`emoji`等脚本语言

`Markdown`是由programmer community推动流行并一统互联网技术文档（并可用于非技术文档）的`LML`。

### What are Markdown Weak in (When Should not Use Markdown)?

1. complicate typesetting复杂排版： 图文表混排、复杂版面、精确定位等
    1. 针对较复杂的排版一般使用`Word`、`Pages`等富文档编辑软件
    1. 针对出版书籍排版一般使用`InDesign`、`QuarkXPress`、`Affinity Publisher`等专业排版软件
    1. 针对科技论文或书籍一般使用`TeX`或其扩展软件
1. visualization可视化：`markdown`本身的可视化能力弱，需借助第三方工具
1. non-standard未有统一的标准： 方言众多

### When Should Use Markdown?

1. software project, especially in `agile` engineering
1. non-strict typesetting document and presentation
1. e-books and notes
1. application that support `Markdown`
1. static web blog

## How to Learn Markdown?

`Markdown` is so easy to learn and so easy to use, maybe just 30 minutes need to spend.

1. for starter
    1. prepare environment: editor and plugin
    1. select a quick start below or find in Internet
    1. begin to write
1. for advancer
    1. write more and more, read more and more
    1. try more and more features: `drawing`, `formula`, `presentation` and so on
    1. try and find differences in main dialect, especially two `GFM`(`GitHub Flavored Markdown` and `GitLab Flavored Markdown`)
    1. maybe read dialect specification

## Futhermore

### Standards and Specifications

<!-- {standards here} -->

1. [John Gruber's Markdown 1.0.1 - 2004/12/17](https://daringfireball.net/projects/markdown/)
1. [CommonMark](https://commonmark.org/)
1. 🌟 **GitHub Flavored Markdown**
    - [GitHub Flavored Markdown Spec](https://github.github.com/gfm/)
    - [Writing on GitHub](https://help.github.com/en/github/writing-on-github)
    - [GitHub Flavored Markdown Sample](https://github.github.com/github-flavored-markdown/sample_content.html)
    - [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)
1. 🌟 **GitLab Flavored Markdown**
    - [GitLab Markdown](https://docs.gitlab.com/ee/user/markdown.html)
1. [google markdown style](https://github.com/google/styleguide/blob/gh-pages/docguide/style.md)
1. [gitlab documentation guidelines](https://docs.gitlab.com/ee/development/documentation/)
    1. [gitlab documentation style guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html)

### Documents and Supports

<!-- {trusted documents goes here} -->

### Papers and Articles

<!-- {topics goes here} -->

1. [在VSCode中高效编辑markdown的正确方式](https://www.thisfaner.com/p/edit-markdown-efficiently-in-vscode)
    - `vscode`本身对`markdown`的支持
    - extension: `Microsoft/docs-markdown`
    - extension: `shd101wyy/markdown-preview-enhanced`
    - extension: `shd101wyy/crossnote`
    - extension: `markdown-all-in-one`
    - extension: `Prettier/prettier`
    - extension: `DavidAnson/markdownlint`
    - `chrome extension`: `copy as markdown`

### Books and Monographs

<!-- {publishes goes here} -->

### Courses and Tutorials

<!-- {systematic courses goes here} -->

1. [GitHub. Basic writing and formatting syntax](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
1. [CommonMark tutorial](https://commonmark.org/help/)
1. [GitHub mastering Markdown](https://guides.github.com/features/mastering-markdown/)
1. [GitLab a 5-minute Markdown tutorial](https://about.gitlab.com/blog/2018/08/17/gitlab-markdown-tutorial/)

### Manuals and CheatSheets

<!-- {manuals goes here} -->

1. [GitHub Markdown cheatsheet](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)
1. [GitLab Markdown cheatsheet](https://gitlab.com/francoisjacquet/rosariosis/-/wikis/Markdown-Cheatsheet)

### Playgrounds and Exercises

<!-- {exercises here} -->

1. [markdown-it](https://markdown-it.github.io/)
1. [dillinger](https://dillinger.io/)

### Examples and Templates

<!-- {examples here} -->

### Auxiliaries and Tools

<!-- {auxiliaries here} -->

1. 👍 [markdown-preview-enhanced](https://shd101wyy.github.io/markdown-preview-enhanced/#/)
    1. **plugin**
        1. [ATOM plugin](https://atom.io/packages/markdown-preview-enhanced)
        1. [VSCode plugin](https://marketplace.visualstudio.com/items?itemName=shd101wyy.markdown-preview-enhanced)
    1. **docs**
        1. [main docs](https://shd101wyy.github.io/markdown-preview-enhanced/#/)
1. 👍 [marp](https://marp.app/)
    1. **plugin**
        1. [Marp for VS Code](https://marketplace.visualstudio.com/items?itemName=marp-team.marp-vscode)
    1. **announcement**
        1. [marp upgrade to marp-next](https://yhatt.github.io/marp/)
        1. [the story of marp-next](https://marp.app/blog/the-story-of-marp-next)
    1. **docs**
        1. [marpit: markdown slide deck framework](https://marpit.marp.app/)
        1. [built-in themes](https://github.com/marp-team/marp-core/blob/master/themes/README.md)
    1. **example**
        1. [marp-cli-example](https://github.com/yhatt/marp-cli-example)
        1. [marp-next example](https://gist.github.com/yhatt/a7d33a306a87ff634df7bb96aab058b5) , also can see in this repo [marp sample](./Samples/marp.md)
1. [typora](https://typora.io/) :warning: non-free software
1. [marked 2](https://marked2app.com/) :warning: non-free software
1. [mweb](https://www.mweb.im/) :warning: non-free software, `macOS/iOS/ipadOS`-only

### Miscellaneous

<!-- {misc here} -->
