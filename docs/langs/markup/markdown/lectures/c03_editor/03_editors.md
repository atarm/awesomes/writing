# Markdown工具链上的编辑器

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Markdown工具链上的编辑器](#markdown工具链上的编辑器)
    1. [什么是工具链？](#什么是工具链)
    2. [Markdown常用编辑器](#markdown常用编辑器)
    3. [Markdown-Preview-Enhanced](#markdown-preview-enhanced)
    4. [更多的延伸资料](#更多的延伸资料)

<!-- /code_chunk_output -->

## 什么是工具链？
<!-- TODO: Add toolchain explain-->
如果仅仅是单纯的Markdown，那它也不过是一个优秀的写笔记的工具，而不能当成是一种写作工具。

大多数 **Unix-Like** 软件跟 **Win-Based** 软件有不太一样的哲学理念：**Unix-Like** 软件大多数强调 **工具链ToolChain** 的概念，也就是多个工具组合起来使用以完成一个任务，而 **Win-Based** 软件大多数强调 **集成开发环境IDE** 的概念, 单一的软件完成一个任务。即使，现在 **Unix-Like** 软件也有大量的IDE，但很多是通过插件形式以类似工具链的方式加上IDE的外壳（典型的如 **`Eclipse`** ）。

工具链最初指软件开发工具链（典型的如：**`GUN ToolChain`** ），现在也扩大到指将多个工具在 **运行时** 组合共同完成一个任务的工具组合。

工具链的优势大致有：

1. 灵活：
    + 可以组合不同的软件完成不同的任务
    + 工具链中某一环节的工具可以替换成另一个工具达到不同的效果，比如：`ATOM + Markdown + Git + GitBook` 这条工具链可以替换成 `ATOM + ASCiiDoc + Git + GitBook`
1. 迭代更新：当工具链难以满足新的需求时，往往整体更新或替换一条工具链，而是更新其中的某一或某些环节（同时上下游的工具增加兼容性支持）

工具链的劣势主要体现在 **复杂、学习成本高** ，具体体现在：

1. 为完成一个任务，需要学习多个软件
1. 高阶的使用技巧往往不是GUI图形界面上完成的，而是通过命令行、配置文档、快捷指令等完成

## Markdown常用编辑器

Markdown是一种标记语法，大量优秀的编辑器通过插件的形式扩展了对Markdown的支持，也出现了不少新的专门针对Markdown的编辑器。

他们各有优缺点：

1. 插件扩展形式的编辑器：
    + 优点：功能强大，
    + 缺点：学习曲线陡，部分
1. 专门针对Markdown的编辑器：
    + 优点：轻量
    + 缺点：功能单一，难于以编辑器为中心构建工具链

Roming自从选择使用ATOM之后，就比较少关注其他Markdown工具。

之所以选择ATOM是因为ATOM具有以下优势：

1. **VIM模式：** vim和emacs是 **Unix-like** 操作系统下最著名的两款编辑器，VIM号称"编辑器之神"、Emacs号称"神的编辑器"，作为凡人的Roming没有使用过Emacs，VIM也只会非常非常少的操作，但这并妨碍Roming钟情于使用 "VIM模式"， ATOM和Sublime Text都支持vim模式，甚至 **`Chrome`** 浏览器也有很好用的 **`Vimuim`** 插件实现VIM模式的网页浏览。
1. **丰富的插件：** 各种各样的插件，并且由于GitHub的盛名，这样插件还在不断增加。
1. **高逼格的界面：** 丰富的主题插件，其中有不少逼格极高。
1. **几乎无需使用鼠标：** **丰富的快捷键 + 丰富的命令 + VIM模式** ，几乎可以做到双手不离键盘。
1. **GitHub出品：** 强大的发布者聚集最优秀的开发者不断演进这款编辑器

ATOM也有一些缺点：

1. **资源消耗大：**
1. **学习曲线陡：**
1. **需特殊方式安装和更新插件：** 原本ATOM插件的安装和更新非常便捷，但由于一些原因，国内很难安装和更新插件，特别是更新的时候（原因，你懂的。。。）

下面是收集整理自网上关于Markdown常用编辑器

1. `cross-os`:
    1. `visual studio code`
1. macOS：
    - ATOM
    - Sublime Text
    - Ulysses
    - Typora
    - Texpad
    - Mou
    - MacVim
1. Windows：
    - ATOM
    - Sublime Text
    - Vim
    - MarkdownPad
    - MarkPad

## Markdown-Preview-Enhanced

> 注意：如果需要使用GitBook制作电子书，需要注意避免使用MPE语法中的非GFM语法扩展。

## 更多的延伸资料

1. **Markdown官方网页：** https://daringfireball.net/projects/markdown/
1. **Mastering Markdown（英文）：**  https://guides.github.com/features/mastering-markdown/
1. **MPE简体中文帮助：** https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/
1. **MPE语法简体中文：** https://shd101wyy.github.io/markdown-preview-enhanced/#/zh-cn/markdown-basics
1. **Markdown语法说明（简体中文）：** https://coding.net/help/doc/project/markdown.html
1. **macOS文档神器 *Dash* 有Markdown的语法说明（英文）**
1. **GFM规范：** https://github.github.com/gfm/
1. **《GFM规范》发布的简体中文报道：** https://linux.cn/article-8399-1.html
1. **在 *Google* 或 *Baidu* 搜索 *"Markdown"* 获取更多的信息**
