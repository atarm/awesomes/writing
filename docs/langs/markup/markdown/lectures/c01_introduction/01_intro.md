# Markdown介绍

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [Markdown是什么](#markdown是什么)
2. [Markdown的语法简述](#markdown的语法简述)
3. [Markdown的优势及其写作场景](#markdown的优势及其写作场景)
    1. [Markdown的优势](#markdown的优势)
    2. [Markdown适合的写作场景](#markdown适合的写作场景)
4. [Markdown的劣势及不适合的写作场景](#markdown的劣势及不适合的写作场景)
    1. [Markdown的劣势](#markdown的劣势)
    2. [Markdown不适合的写作场景](#markdown不适合的写作场景)
5. [延伸资料](#延伸资料)
6. [帮助文档](#帮助文档)

<!-- /code_chunk_output -->

<center><div style="width: 100px;">

![choose-markdown](./.assets/image/markdown_logo.jpg)

</div></center>

> 使用Word进行长文本写作是一件极其痛苦的事，速度慢、文件大、文档风格难统一、排版效率低、容易崩溃、版本控制难等等，反正一堆难于忍受的缺点。
> 如果不想再忍受Word的种种缺点，不妨试试Markdown这种简单的轻量标记语言和与它相关的一系列可选工具链，或许会重新喜欢上写作或写笔记。

## Markdown是什么

2004年， **John Gruber** 与 **Aaron Swartz** 协同发布了Markdown这种为网络写作而生的轻量标记语法。很快地，随着开源程序员广泛使用，Markdown现在几乎成了程序员们的标配，目前最大的开源聚集地 **github.com** 到处弥漫着Markdown的身影。

Markdown是一种轻量标记语法，可以看成是 **HTML** 标记语言的一种简化，跟HTML一样，Markdown也以纯文本形式存储。

Markdown作为一种开源的语法，慢慢地就有大量的编辑器（现有的编辑器或新增的编辑器）支持它。

使用Markdown写作的文档一般会实时预览渲染成HTML文档进行查看，这样，即可以专心地进行文字写作不必分心调整格式和样式，又可以实时地看到排版的效果。

还可以将多个Markdown文档整理成电子书（`EPUB`, `PDF`, `MOBI`等格式）。

不仅程序员使用Markdown， **简书（www.jianshu.com/ ）** 也是国内使用Markdown的一个好例子。微信公众号文章也可以先使用Markdown写作后再将本地预览以 **`Ctrl - C`** 和 **`Ctrl - V`** 的方式快速排版（需要简单重新调整和上传图片）。

**下图是Roming在本地书写本文的截图（左边书写，右边实时预览，最右边显示 _`TODO`（待办）_ 事项）：**
<!-- FIXME: Maybe change difference picture -->

![markdown_writing_env](./.assets/image/markdown_writing_env.jpg)

**下图是将本文的源文件经过`CTRL - C` + `CTRL - V`到简书的截图：**

![jianshu_writing_env](./.assets/image/jianshu_writing_env.jpg)

## Markdown的语法简述

自从Markdown发布以来，就有不少对Markdown语法进行扩展，几乎每一款常用的Markdown编辑器都或多或少地进行扩展，**`GitHub Flavored Markdown(GFM)`** 是其中重要的扩展之一。

网上关于Markdown语法的描述非常多，在任何一个搜索引擎中搜索 "Markdown语法" 都可以找到类似的语法说明。

如果有意于使用Markdown，建议仔细阅读下文"更多的延伸资料"链接中的 **"Mastering Markdown（英文）"** 和 **"Markdown语法说明（简体中文）"**。

下面是一个简单的语法例子：

**源文件：**

```markdown{.line-numbers}
<!-- 字体标记 -->
粗体： **粗体** 或 __粗体__
斜体： *斜体* 或 _斜体_

<!-- 列表标记 -->
1. 有序列表项1
1. 有序列表项2
1. 有序列表项3

+ 无序列表项1
+ 无序列表项2
+ 无序列表项3

<!-- 链接 -->
[GitHub链接](http://github.com)

<!-- font awesome -->
:fa-car:    :fa-github:     :fa-github-alt:
```

**渲染后：**

粗体： **粗体** 或 __粗体__
斜体： *斜体* 或 _斜体_

1. 有序列表项1
1. 有序列表项2
1. 有序列表项3

+ 无序列表项1
+ 无序列表项2
+ 无序列表项3

[GitHub链接](http://github.com)

:fa-car:    :fa-github:     :fa-github-alt:

>备注：微信公众号和简书不支持书写 **`Font Awesome`**

## Markdown的优势及其写作场景

### Markdown的优势

1. **纯文本格式：**
    + 几乎任何工具都可以打开Markdown文档查看里面的内容
    + 文件小：同样 <!-- TODO: -->
    + 版本控制方便，不像Word这种二进制或压缩包文档，每一次版本更新都得整个文档提交并且难于跟综每一次的修改（事实上大部分写作者都会为一份文稿存储多个不同的文档并起不同名字以示不同版本的区别）
1. **语法简单：** Markdown的语法非常简单，只需 **10几分钟** 就可以上手
1. **CSS排版样式：** 就是 HTML文档一样，配套不同的CSS样式文件就可以展示不同排版效果（Roming使用的CSS样式就是自定义的样式，其中的 **自动编号** 也是CSS的功能）
1. **HTML兼容：** 当Markdown的无法满足特殊需要时，可以使用HTML标签进行定义，当然一般并不建议大量使用HTML标签，如果必须大量使用HTML标签建议使用 **DIV + CSS** 进行排版，这样可控性和结构性更好
1. **工具链丰富：** 像绝大多数开源工具一样，Markdown也不是一个孤立的语言或工具，Markdown可以整合Git、PlantUML、GitBook、Mindmap等大量工具

### Markdown适合的写作场景

1. 发布在网络上的文档
1. 电子书
1. 笔记
1. 软件说明书

## Markdown的劣势及不适合的写作场景

### Markdown的劣势

1. **（排版功能）过于简单：** 视乎不同的任务，简单即是Markdown的优点，也是Markdown的缺点，虽然可以通过CSS样式定义不同的样式，然后在不同章节、段落套用相应的样式，但这违背了Markdown的初衷，书写起来也会变得异常复杂
1. **标准不统一：** 虽然Markdown也制定了 **CommonMark** 标准，但实际上这个标准并没有太大作用

### Markdown不适合的写作场景

1. 纸质出版物（纸质书籍、论文等）
1. 格式样式复杂的文档（大量不同字体、复杂表格等）
1. 版式复杂的文档（大量分栏、不同章节不同版式等）
1. 行政文档（公司公文等）

## 延伸资料

1. **Markdown官方网页：** <https://daringfireball.net/projects/markdown/>
1. **Mastering Markdown（英文）：**  <https://guides.github.com/features/mastering-markdown/>
1. **Markdown语法说明（简体中文）：** <https://coding.net/help/doc/project/markdown.html>
1. **GFM规范：** <https://github.github.com/gfm/>
1. **《GFM规范》发布的简体中文报道：** <https://linux.cn/article-8399-1.html>

## 帮助文档

1. [Markdown Syntax by John Gruber](https://daringfireball.net/projects/markdown/syntax#)
1. <https://devdocs.io/markdown/>
