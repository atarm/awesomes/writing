# Markdown基本语法

<!-- @import "[TOC]" {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->
<!-- code_chunk_output -->

1. [主题摘要](#-主题摘要)
2. [Markdown语法与Markdown编辑器](#-markdown语法与markdown编辑器)
3. [Markdown语法组成结构](#-markdown语法组成结构)
4. [Markdown字符格式](#-markdown字符格式)
    1. [粗体](#-粗体)
    2. [斜体](#-斜体)
    3. [行内代码](#-行内代码)
    4. [链接](#-链接)
5. [Markdown段落格式](#-markdown段落格式)
    1. [标题](#-标题)
    2. [列表](#-列表)
        1. [列表的类型](#-列表的类型)
        2. [有序列表](#-有序列表)
        3. [无序列表](#-无序列表)
    3. [引用](#-引用)
    4. [图片](#-图片)
    5. [分隔行](#-分隔行)
    6. [代码块](#-代码块)
    7. [表格](#-表格)
6. [其他](#-其他)
    1. [`HTML entities`](#-html-entities)

<!-- /code_chunk_output -->

## 主题摘要

用简短的篇幅介绍`Markdown`语法组成结构和基本语法。

## Markdown语法与Markdown编辑器

`Markdown`是一种`标记语法`，这种`标记语法`需要使用相应的`Markdown渲染器`（或称为`Markdown解析器`或`Markdown编译器`）将`Markdown源文件`渲染成最终可读的效果（ **类似`浏览器引擎`将`HTML源文件`渲染成`网页`** ）。

`Markdown编辑器`就是可以输入`Markdown源文件`并有相应的`Markdown渲染器`渲染成最终可读效果的一种工具，一般情况下，`Markdown编辑器`都具有`实时预览`的功能（每次输入或修改`Markdown源文件`都会即时渲染成最终可读效果）。

## Markdown语法组成结构

`排版`主要包括`版面格式`、`段落格式`、`字符格式`三种组成结构，`Markdown`本身没有`版面格式`的语法，不过很多`markdown编辑工具`可以通过`CSS`进行`版面格式`设置（比如：`ATOM`的`MPE`插件）。

由于`Markdown`是一种使用`标记语法`来定义`格式`的`排版语法`，因此，就由`格式定义字符`来定义`格式`：

1. **`特殊定符`：** 
    1. **`格式定义字符`：** 用于定义格式的特殊字符，`Markdown`的`格式定义字符`有<code>\`</code> 、`*` 、`_` 、`()`、`[]`、`#`、`+`、`-`、`.`、`!`（其他扩展的`Markdown渲染器`可能有`^`、`~`、`:`等）
    1. **`转义字符`：** 用于转义并显示特殊字符（而不是使用`特殊字符`来定义格式），`Markdown`的转义字符是`\`（在`Markdown`中并不是出现了`特殊字符`就必须转义才能显示，而是在`特殊字符`在`定义上下文`中才需要`显式转义`）
1. **`字符格式`：** 也称为`行内格式`，用于行内某些字符的格式或行为
1. **`段落格式`：** 用于一段或多段之间的格式
1. **`内嵌HTML`：** `Markdown`可以直接使用`内嵌HTML`来定义`Markdown`支持或不支持的结构

**注意：在`排版`中的`段落`(`Paragraph`)和`换行`(`Line Break`)是两个不同的概念，`换行`不一定换`段落`（在`Word`里`换行`即换`段落`），`段落`是一个`排版`元素，同一个`段落`有共同的`段落格式`（多个`段落`也可以使用相同的`段落格式`），一个`段落`可以包括`多行`。`段落`和`多行`之间的一个明显差别就是不同的`段落`之间使用的是`段落间距`，而`多行`之间使用的是`行间距`（一般情况下，`段落间距`会比`行间距`大）。**

**在最初的`Markdown`中进行`换行`需要使用`<br/>标签`或在行末增加`两个或以上的空格`，否则中间没有`空行`（什么都没有或只有`空格`或`TAB符`）的两行会显示在一行中（与`HTML源文件`的渲染效果一样）。而中间有`空行`的多行即是不同的段落**

**在`GFM`扩展中，不需要使用`<br/>标签`或在行末增加`两个或以上的空格`也可以显示为`多行`，但这多行是同一个`段落`，只有中间有`空行`的`多行`才是不同的`段落`。**

## Markdown字符格式

### 粗体

使用`**`或`__`（两个`下划线`字符）包围的字符将被渲染成粗体。

**`粗体`的`Markdown`源文件：**

```markdown{.line-numbers}
**这是粗体**

__这也是粗体__
```

**`粗体`的渲染效果：**

**这是粗体**

__这也是粗体__

### 斜体

使用`*`或`_`包围的字符将被渲染成斜体

**`斜体`的`Markdown`源文件：**

```markdown{.line-numbers}
*这是斜体*

_这也是斜体_
```

**`斜体`的渲染效果：**

*这是斜体*

_这也是斜体_

**备注：中文字符一般不使用斜体，因为中文的斜体不怎么好看**

### 行内代码

使用 **\`** （键盘中`~`字符所在的按键）包围的字符将被渲染成`行内代码`（`HTML`里的`<code>标签`）。

**`行内代码`的`Markdown`源文件：**
```markdown{.line-numbers}
这是`行内代码`
```

**`行内代码`的渲染效果：**
这是`行内代码`

### 链接

`Markdown`可以使用`inline-sytle`和`reference-style`两种方式定义`链接`。

**`inline-style`** 的语法：`[link-text](url-link "optional-tips")`，其中：

1. **`link-text`：** 显示的 `链接文本`
1. **`url-link`：** `url链接`
1. **`optional-tips`：** 当`鼠标`放置在`link-text`上显示的`提示`

**`reference-style`** 的语法包括两部分：

1. **`[link-text][reference-id]`** 
1. **`[reference-id]:url-link "optional-tips"`：** 在同一个文件的其他段落定义

**`链接`的`Markdown`源文件：**

```markdown{.line-numbers}
这是[百度搜索](www.baidu.com "使用百度进行搜索")inline-style

这是[百度搜索][baidu]reference-style

[baidu]:www.baidu.com "使用百度进行搜索"
```

**`链接`的渲染效果：**
这是[百度搜索](www.baidu.com  "使用百度进行搜索")inline-style

这是[百度搜索][baidu]reference-style

[baidu]:www.baidu.com "使用百度进行搜索"

## Markdown段落格式

### 标题

`Markdown`支持两种标题定义方式：

1. `setext-style`： **支持一级和二级标题**，通过在单独的一行中定义标题格式，多于一个`=`字符定义为`一级标题`、多于一个`-`字符表示`二级标题`
1. `atx-style`： **支持一级至六级标题**，通过在标题行前的`#`字符定义标题，一个`#`定义`一级标题`、两个`#`定义`二级标题`，以此类推支持最多定义到`六级标题`

**`setext-style`标题的`Markdown`源文件：**

```markdown {.line-numbers}
这是一级标题
=

这是二级标题
-
```

**`atx-style`的`Markdown`源文件：**

```markdown {.line-numbers}
# 这是一级标题

## 这是二级标题

###### atx-style支持到六级标题
```

**建议使用`atx-style`定义标题，一方面`atx-style`可以支持至`六级标题`，另一方面`atx-style`不需要使用单独一行定义标题格式。**

### 列表

#### 列表的类型

列表分为`有序列表`和`无序列表`两种基本列表，列表之间可以嵌套（通过增加`1个TAB`或`4个空格`的`缩进`）形成`多级列表`。

#### 有序列表

使用`非负数字`+`.`+`一个或以上空格`定义`有序列表`，`非负数字`不必要从`1`递增，`Markdown`在渲染时会从`1`开始自动递增（当`有序列表`的第一行是`0.`时从`0`开始递增，`多级列表`的第二级开始不支持从`0`开始）。

**有序列表的`Markdown`源文件：**

```markdown {.line-numbers}
0. 这是一级有序列表
    1. 这是二级有序列表
        1. 这是三级有序列表
8. 这是一级有序列表
0. 这是一级有序列表
```

**有序列表的渲染效果：**
0. 这是一级有序列表
    1. 这是二级有序列表
        1. 这是三级有序列表
8. 这是一级有序列表
0. 这是一级有序列表

#### 无序列表

使用一个`*`或`+`或`-`加上`一个以上空格`定义`无序列表`。

**无序列表的`Markdown`源文件：**

```markdown {.line-numbers}
* 这是无序列表
+ 这是无序列表
- 这是无序列表

+ 这是一级无序列表
    1. 这是二级有序列表
    1. 这是二级有序列表
+ 这是一级无序列表
```

**无序列表的渲染效果：**

* 这是无序列表
+ 这是无序列表
- 这是无序列表

+ 这是一级无序列表
    1. 这是二级有序列表
    1. 这是二级有序列表
+ 这是一级无序列表

### 引用

`Markdown`使用`email-style`（`>`字符）定义`引用`，`引用`段落有条竖线展示这是一个`引用`段落，`引用`段落可以是多级的。

**引用的`Markdown`源文件：**

```markdown{.line-numbers}
>这是一级引用
>>这是二级引用
>>>这是三级引用
>>这还是三级引用
>这还是三级引用

>新的引用段落

```

**引用的渲染效果：**
>这是一级引用
>>这是二级引用
>>>这是三级引用
>>这还是三级引用
>这还是三级引用

>新的引用段落

### 图片

`Markdown`中嵌入`图片`的语法跟`链接`的语法类似，差别在于嵌入`图片`比嵌入`链接`前多了个`!`特殊字符表示这是一个指向`图片`的`url`。

**图片的`Markdown`源文件：**

```markdown{.line-numbers}
这是一张使用inline-style链接的图片

![当图片无法显示时出现这个文本](image-url "optional-tips")


这是一张使用reference-style链接的图片
![当图片无法显示时出现这个文本][reference-id]

[reference-id]:image-url "optional-tips"
```

**`图片链接`最好放在一个单独的`段落`中（而不仅仅是不同的行）以便正常显示，因为如果跟`文本`在同一个`段落`中，有可能因为`行高`不够而无法将`图片`显示完整。**

### 分隔行

在单独的一个段落中使用三个或多于三个`*`或`-`或`_`将被渲染成`分隔行`。

### 代码块

被 **\`\`\`** 包围的段落称为`代码块`（有些`Markdown`解释器支持`语法高亮`）。

### 表格

**`Markdown`之父 **John Gruber** 和 **Aaron Swartz** 发布的`Markdown`解释器中没有`表格格式`，目前一般使用的`表格格式`是`GFM`的扩展**

**表格的`Markdown`源文件：**

```markdown{.line-numbers}
|表格字段一|表格字段二|表格字段三|表格字段四|
|--|:--|--:|:--:|
|默认居中对齐|左对齐|右对齐|居中对齐|
|cell|cell|cell|cell|
```

**表格的渲染效果：**

|表格字段01|表格字段02|表格字段03|表格字段04|
|--|:--|--:|:--:|
|默认左对齐|左对齐|右对齐|居中对齐|
|cell|cell|cell|cell|

## 其他

### `HTML entities`
