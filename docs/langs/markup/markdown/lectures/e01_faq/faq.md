# FAQ

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [如何在内联代码或代码块中包含反引号❓](#如何在内联代码或代码块中包含反引号)

<!-- /code_chunk_output -->

## 如何在内联代码或代码块中包含反引号❓

原则：

1. 作为开始符或结束符的反引号的数量应多于被包含的反引号的数量
1. 作为开始符或结束符的反引号如果紧接着就是被包含的反引号时，开始符和结束符与被包含的反引号之间应有空白字符
1. 开始符和结束符的反引号数量应相同

示例：

```markdown {.line-numbers}
内联代码：``中间是`反引号``
```

内联代码：``中间是`反引号``

```markdown {.line-numbers}
内联代码：只有一个反引号`` ` ``
```

内联代码：只有一个反引号`` ` ``

```markdown {.line-numbers}
内联代码：只有两个反引号``` `` ```
```

内联代码：只有两个反引号``` `` ```

`````markdown {.line-numbers}
````md
代码块：四个反引号作为开始符和结束符里包含三个反引号```
````
`````

````md
代码块：四个反引号作为开始符和结束符里包含三个反引号```
````

>[Markdown 如何在内联代码或者代码块中使用代码开始符号反引号（`）](https://blog.walterlv.com/post/markdown-code-escape-backtick.html)
