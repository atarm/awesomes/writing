# {course_name}

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [关于本仓库About](#关于本仓库about)
    1. [许可协议Licenses](#许可协议licenses)
    2. [目录组织Structure](#目录组织structure)
    3. [帮助完善Contribution](#帮助完善contribution)
2. [Bibliographies参考资料](#bibliographies参考资料)
    1. [书籍Books](#书籍books)
    2. [课程Courses](#课程courses)
    3. [文档Documentations](#文档documentations)

<!-- /code_chunk_output -->

## 关于本仓库About

### 许可协议Licenses

![CC-BY-SA-4.0](./.assets/LICENSES/cc-by-sa-4.0/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

<!--

### 编写环境和使用方法

请详见<https://gitlab.com/arm_commons/commons/-/blob/master/ENVS/MD_RAW/README.md>
-->

### 目录组织Structure

```bash {.line-numbers}
.
├── .editorconfig       ==> EditorConfig
├── .git                ==> git repository
├── .gitignore          ==> gitignore
├── README.md           ==> 本文件
├── codes               ==> 代码
└── docs                ==> 文档
    ├── abouts          ==> 相关介绍，如：大纲等
    ├── addons          ==> 附加内容，如：专题论述等
    ├── exercises       ==> 习题及其解析
    ├── experiments     ==> 实践指引及FAQ
    ├── glossaries      ==> 术语表
    ├── lectures        ==> 讲义
    └── slides          ==> 幻灯片/课件/PPT（在`VSCode`上使用`MARP`编辑）
```

### 帮助完善Contribution

期待您一起完善，您可以通过以下方式帮助完善：

1. **`merge request`**：通过`GitLab`的`merge request`到`master`分支
1. **`issue`**： 通过`GitLab`的`issue`发起一个新的`issue`（标签设置成`optimize`）

仓库地址：

1. 主地址： {repository_address}
1. 镜像地址： {repository_address}

## Bibliographies参考资料

### 书籍Books

{books}

### 课程Courses

{courses}

### 文档Documentations

{docs}
